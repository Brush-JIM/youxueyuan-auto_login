# 优学院自动登录-YouXueYuan-Auto_Login

项目地址  
GitHub:[https://github.com/Brush-JIM/YouXueYuan-Auto_Login](https://github.com/Brush-JIM/YouXueYuan-Auto_Login)  
Bitbucket:[https://bitbucket.org/Brush-JIM/youxueyuan-auto_login/](https://bitbucket.org/Brush-JIM/youxueyuan-auto_login/)  
GreasyFork:[https://greasyfork.org/zh-CN/scripts/381809-优校园自动登录](https://greasyfork.org/zh-CN/scripts/381809-优校园自动登录)
  
# 食用方法  
* 经过测试，脚本不适用Greasemonkey；我看了Greasemonkey的文档还是不知道怎么使用GM.* 函数，因为我使用文档的例子一样失败，有大神知道怎么用GM.setValue、GM.getValue、GM.deleteValue吗？  
* 访问  
    GitHub:[https://raw.githubusercontent.com/Brush-JIM/YouXueYuan-Auto_Login/master/优学院自动登录.user.js](https://raw.githubusercontent.com/Brush-JIM/YouXueYuan-Auto_Login/master/优学院自动登录.user.js)  
    or  
    Bitbucket:[https://bitbucket.org/Brush-JIM/youxueyuan-auto_login/src/master/优学院自动登录.user.js](https://bitbucket.org/Brush-JIM/youxueyuan-auto_login/src/master/优学院自动登录.user.js)  
    or  
    GreasyFork:[https://greasyfork.org/zh-CN/scripts/381809-优校园自动登录](https://greasyfork.org/zh-CN/scripts/381809-优校园自动登录)  
  安装脚本“优学院自动登录.user.js”到Tampermonkey/Violentmonkey/Greasemonkey中  

# 注意
* 脚本依赖油猴实现  
  本人使用Tampermonkey  
  Google chrome请安装[Tampermonkey](https://tampermonkey.net/)或[Violentmonkey](https://violentmonkey.github.io/)  
  Firefox请安装[Tampermonkey](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)或[Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)或[Violentmonkey](https://addons.mozilla.org/zh-CN/firefox/addon/violentmonkey/)  
  其他浏览器请看[https://greasyfork.org/zh-CN](https://greasyfork.org/zh-CN)  
* 理论上无论是Tampermonkey还是Violentmonkey抑或是Greasemonkey，脚本都是互通的，都可以用，只是理论上！请自行测试  
* 注意！脚本使用了Tampermonkey的GM_* 函数；而Greasemonkey中是使用GM.* 函数，但我没测试过，所以没有增加GM.* 函数。Greasemonkey下使用可能会有问题

# 日志
CHANGELOG.md [GitHub](https://github.com/Brush-JIM/YouXueYuan-Auto_Login/blob/master/CHANGELOG.md) or [Bitbucket](https://bitbucket.org/Brush-JIM/youxueyuan-auto_login/src/master/CHANGELOG.md?fileviewer=file-view-default)  
  
# 最后  
我读的专业不是编程方向的，纯靠兴趣爱好  
且js没学过，技术纯属渣渣  
所以出现问题请不要大喷  
  
---
代码开源、免费  
不接受任何捐赠  
