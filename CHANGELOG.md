# 日志  

## 19.4.7 V0.1
* 上传第一份  
* 其实很早之前就开始写，只是懒得完善，现在放假，于是完善代码  
* 注意！脚本使用了Tampermonkey的GM_setValue、 GM_getValue、GM_deleteValue；而Greasemonkey中是使用GM.setValue、GM.getValue、GM.deleteValue，但我没测试过，所以没有增加GM.setValue、GM.getValue、GM.deleteValue。所以Greasemonkey下使用可能会有问题。所以我没有将这个项目合并到看视频项目，以防影响其他功能（虽然理论上不会），也为了减少维护难度。