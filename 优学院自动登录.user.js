// ==UserScript==
// @name         优学院自动登录
// @namespace    https://github.com/Brush-JIM/YouXurYuan-Auto_Login
// @version      0.1
// @description  优学院自动登录 GitHub:https://github.com/Brush-JIM/YouXurYuan-Auto_Login 或者Bitbucket:https://bitbucket.org/Brush-JIM/youxuryuan-auto_login/
// @author       Brush-JIM
// @match        https://*.ulearning.cn/ulearning/index.html*
// @match        https://*.ulearning.cn/organization/index.html*
// @match        https://www.ulearning.cn/umooc/user/login.do
// @grant        unsafeWindow
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @icon         https://www.ulearning.cn/ulearning/favicon.ico
// ==/UserScript==

(function() {
    'use strict';
    if (window.location.href.search("https://www.ulearning.cn/umooc/user/login.do") != -1)
    {
        window.location.href = 'https://www.ulearning.cn/ulearning/index.html#/index/portal';
    }
    else if (window.location.href.search("/ulearning/index.html#/delete_auto_login") != -1)
    {
        GM_setValue('username', null);
        GM_setValue('password', null);
        alert('重置成功');
        window.location.href='https://www.ulearning.cn/ulearning/index.html';
    }
    else if (window.location.href.search("ulearning.cn/ulearning/index.html") != -1 || window.location.href.search("ulearning.cn/organization/index.html") != -1)
    {
        var username = GM_getValue('username');
        var password = GM_getValue('password');
        if (username == null || password == null || username == '' || password == '')
        {
            username = prompt("请输入账号");
            password = prompt("请输入密码");
            GM_setValue('username', username);
            GM_setValue('password', password);
            if (username == null || username == '' || password == null || password == '')
            {
                alert("账号或密码为空！");
            }
            else
            {
                let count = 6
                let intervalKey = setInterval(() => {
                    if (document.querySelector("[data-bind='click: login, text: publicI18nText.signin']") != null)
                    {
                        document.querySelector("[data-bind='click: login, text: publicI18nText.signin']").click();
                        document.querySelector("input[id='userLoginName']").value = username;
                        document.querySelector("input[id='userPassword']").value = password;
                        document.querySelector("button[class='button button-red-solid btn-confirm']").click();
                        if (document.documentElement.innerHTML.search('<div class="student-tip" style="display: block;">用户名/密码错误</div>') != -1)
                        {
                            alert('账号或密码错误！');
                            GM_setValue('username', null);
                            GM_setValue('password', null);
                        }
                        clearInterval(intervalKey);
                    }
                    if (--count === 0)
                    {
                        clearInterval(intervalKey);
                    }
                }, 1 * 1e3);
            }
        }
        else
        {
            let count = 6
            let intervalKey = setInterval(() => {
                if (document.querySelector("[data-bind='click: login, text: publicI18nText.signin']") != null)
                {
                    document.querySelector("[data-bind='click: login, text: publicI18nText.signin']").click();
                    document.querySelector("input[id='userLoginName']").value = username;
                    document.querySelector("input[id='userPassword']").value = password;
                    document.querySelector("button[class='button button-red-solid btn-confirm']").click();
                    if (document.documentElement.innerHTML.search('<div class="student-tip" style="display: block;">用户名/密码错误</div>') != -1)
                    {
                        alert('账号或密码错误！');
                        GM_setValue('username', null);
                        GM_setValue('password', null);
                    }
                    clearInterval(intervalKey);
                }
                if (--count === 0)
                {
                    clearInterval(intervalKey);
                }
            }, 1 * 1e3);
        }
        //增加重置登录信息
        let count = 6
        let intervalKey = setInterval(() => {
            if (document.querySelector("[data-bind='click: login, text: publicI18nText.signin']") != null)
            {
                clearInterval(intervalKey);
            }
            if (--count === 0)
            {
                var a = document.createElement("a");
                a.setAttribute("class", "menu-item");
                a.setAttribute("href", "javascript:window.location.replace('https://www.ulearning.cn/ulearning/index.html#/delete_auto_login'); window.location.reload();");
                a.setAttribute("onclick", "function(){window.location.href='https://www.ulearning.cn/ulearning/index.html#/delete_auto_login'; window.location.reload();}");
                a.innerHTML='重置登录信息';
                document.querySelector("div[class='nav-menu pull-left']").appendChild(a);
                clearInterval(intervalKey);
            }
        }, 0.3 * 1e3);
        //删除一些元素，改善排版
        count = 6
        intervalKey = setInterval(() => {
            if (document.querySelector("a[class='down-app-a']") != null)
            {
                for (var i = 0;i < document.querySelectorAll("a[class='down-app-a']").length;i++)
                {
                    document.querySelector("a[class='down-app-a']").parentNode.removeChild(document.querySelector("a[class='down-app-a']"));
                }
            }
        }, 0.3 * 1e3);
    }
})();